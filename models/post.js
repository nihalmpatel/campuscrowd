const mongoose = require('mongoose');

var postSchema = mongoose.Schema({
    body       : String,
    img        : [],
    uploader   : {id: String, name: String}, 
    likes      : [],
    comments   : [{id: String, name: String, body: String},{timestamps: true}]
},{timestamps: true});


module.exports = mongoose.model('post', postSchema);