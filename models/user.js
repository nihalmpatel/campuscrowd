// app/models/user.js

const mongoose = require('mongoose');
const bcrypt   = require('bcryptjs');
const jwt      = require('jsonwebtoken');
const configDB = require('../config/database.js');

var UserSchema = mongoose.Schema({

    username    : {type:String, unique:true, required: true} ,
    password    : {type:String, required: true},
    email       : {type:String, unique:true, required: true} ,
    profile     : {
      avatar:     String,
      firstname:  String,
      lastname:   String,
      college:    String,
      branch:     String,
      year:       Number,
      city:       String,
      state:      String,
      country:    String,
      bio:        String
    },
    network      : [],
    posts        : [],
    sentrequests : [],
    recievedrequests: [],
});


const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback){
  User.findById(id, callback);
}

module.exports.getUserByUsername = function(username, callback){
  const query = {username: username}
  User.findOne(query, callback);
}

module.exports.addUser = function(newUser, callback){
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if (err) throw err;
      newUser.password = hash;
      newUser.save(callback);
    });
  });
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
}