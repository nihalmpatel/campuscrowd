const express = require('express');
const router = express.Router();
const passport = require('passport');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');
const mkdirp = require('mkdirp')

// Get users

router.get('/', (req,res)=>{
    User.find('',(err,users)=>{
        if (err) throw err;
        res.json(users);
    });
});

router.get('/:id', (req,res) => {
    User.getUserById(req.params.id, (err,user) => {
        if (err) {
            res.json({error:'db_error'});
            throw err;
        }
        else {
            res.json(user);
        }
    });
});

// register user

router.post('/register', (req, res) => {

    User.findOne({'username':req.body.username},function(err,user){
        if (err) throw err;

        //checking if user already exists 
        else if (user){
            res.json({error: 'invalid_username'});
        } 

        else {   
            // check if email id already exists
            User.findOne({'email':req.body.email},function(err,user){
                if (err) throw err;

                else if (user){
                    res.render({error: 'invalid_email'});
                }

                // if everything is fine, register the user
                else {
                    let newUser = new User({
                        username : req.body.username,
                        password : req.body.password,
                        email : req.body.email, 
                    });

                    User.addUser(newUser, (err, user) => {
                        if(err){
                        console.log(err)
                        res.json({error:'db_error'});
                        } else {
                        res.json({success: true});
                        }
                    });
                }
            }); 
        }      
    });

});

// Authenticate
router.post('/authenticate', (req, res) => {

  User.getUserByUsername(req.body.username, (err, user) => {
    if(err) throw err;

    if(!user){
      return res.json({error: 'invalid_username'});
    }

    User.comparePassword(req.body.password, user.password, (err, isMatch) => {
        if(err) throw err;
        if(isMatch){
            
            var fullname;
            var profile=false;
            const token = jwt.sign({data:user}, config.secret, {expiresIn: 86400});
            if(user.profile.firstname || user.profile.lastname) {
                profile=true;
                fullname=user.profile.firstname+' '+user.profile.lastname;
            }
            res.json({
            success: true,
            token: 'JWT '+token,
            
            user: {
                id: user._id,
                username: user.username,
                email: user.email,
                avatar:user.profile.avatar,
                fullname: fullname,
                profile: profile
            }
            });
      } 
      else {
        return res.json({error: 'invalid_password'});
      }
    });
  });
});

// Create Profile
router.post('/createprofile',(req,res)=>{
    console.log(req.body);
    User.getUserById(req.body.userid, (err,user) => {
        if(err) throw err;
        
        if(!user){
            return res.json({error: 'invalid_username'});
        }

        profile=req.body;
        delete profile.userid;

        
        user.profile=profile;
        user.save(err=>{
            if (err) {
                res.json({error:'db_error'});
                throw err;
            }
            else {
                res.json({success: true});
            }
        });
        
    });
});

// Avatar upload
router.post('/uploadavatar',(req,res)=>{
    avatarfile='';
    var storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'uploads/')
        },
        filename: function (req, file, cb) {
            avatarfile=file.fieldname + '-' + Date.now() + '.jpg';
            cb(null, avatarfile)
        }
    });
    
    var upload = multer({ storage: storage }).single('avatar');

    upload(req, res, function (err) {
        if (err) {
            res.json({error:'avatar_upload'});
        }
        res.json({success: true,filename:avatarfile});
    });

});

// Send connection request
router.post('/addconnection',(req,res)=>{
    User.getUserById(req.body.from,(err,userFrom)=>{
        if(err) throw err;
        
        if(!userFrom){
            return res.json({error: 'invalid_user'});
        }

        userFrom.sentrequests.push(req.body.to);
        userFrom.save();

        User.getUserById(req.body.to,(err,userTo)=>{
            if(err) throw err;
            
            if(!userTo){
                return res.json({error: 'invalid_user'});
            }
            
            userTo.recievedrequests.push(req.body.from);
            userTo.save();

            res.json({success:true});
        });

    });
});

// Cancle connection request
router.post('/cancleconnection',(req,res)=>{
    User.getUserById(req.body.from,(err,userFrom)=>{
        if(err) throw err;
        
        if(!userFrom){
            return res.json({error: 'invalid_user'});
        }

        var index = userFrom.sentrequests.indexOf(req.body.to);
        if (index > -1) {
            userFrom.sentrequests.splice(index, 1);
        }
        else {
            return res.json({error: 'invalid_connection'});
        }

        userFrom.save();

        User.getUserById(req.body.to,(err,userTo)=>{
            if(err) throw err;
            
            if(!userTo){
                return res.json({error: 'invalid_user'});
            }
            
            var index = userTo.recievedrequests.indexOf(req.body.from);
            if (index > -1) {
                userTo.recievedrequests.splice(index, 1);
            }
            else {
                return res.json({error: 'invalid_connection'});
            }

            userTo.save();

            res.json({success:true});
        });

    });
});

router.post('/acceptconnection',(req,res)=>{
    User.getUserById(req.body.from,(err,userFrom)=>{
        if(err) throw err;
        
        if(!userFrom){
            return res.json({error: 'invalid_user'});
        }

        // removing from sent request
        var index = userFrom.recievedrequests.indexOf(req.body.to);
        if (index > -1) {
            userFrom.recievedrequests.splice(index, 1);
        }
        else {
            return res.json({error: 'invalid_connection'});
        }

        // adding to network
        userFrom.network.push(req.body.to);
        userFrom.save();

        User.getUserById(req.body.to,(err,userTo)=>{
            if(err) throw err;
            
            if(!userTo){
                return res.json({error: 'invalid_user'});
            }
            
            var index = userTo.sentrequests.indexOf(req.body.from);
            if (index > -1) {
                userTo.sentrequests.splice(index, 1);
            }
            else {
                return res.json({error: 'invalid_connection'});
            }

            userTo.network.push(req.body.from);
            userTo.save();

            res.json({success:true});
        });

    });
});

router.get('/connectionrequests/:id',(req,res)=>{
    User.getUserById(req.params.id,(err,user)=>{
        if(err) throw err;
        
        if(!user){
            return res.json({error: 'invalid_user'});
        }

        User.find( { _id: {$in:user.recievedrequests} }, { "username":1, "profile":1 } , (err,users) => {
            if (err) throw err;
            res.json(users);
        });

    });
});

router.get('/network/:id', (req,res) => {
    User.getUserById(req.params.id, (err,user) => {

        if (err) {
            res.json({error:'db_error'});
            throw err;
        }
        
        User.find( { _id: {$in:user.network} }, { "username":1, "profile":1 } , (err,users) => {
            if (err) throw err;
            res.json(users);
        });

    });
});

module.exports = router;