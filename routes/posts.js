const express = require('express');
const router = express.Router();
const passport = require('passport');
const multer = require('multer');
const Post = require('../models/post');
const User = require('../models/user');

router.post('/add', (req,res)=>{
    let post = new Post({
        body       : req.body.body,
        img        : req.body.img ,
        uploader   : {id: req.body.id, name: req.body.name}
    });

    post.save((err)=>{
        if (err) {
            throw err;
            res.json({error:"db_error"});
        }

        User.getUserById(req.body.id,(err,user)=>{
            if (err) throw err;
            user.posts.push(post.id);
            user.save()
        });
        res.json({success:true});
    })
});

router.get('/', (req,res)=>{
    Post.find('',(err,posts)=>{
        if (err) throw err;
        res.json(posts);
    });
});

module.exports = router;