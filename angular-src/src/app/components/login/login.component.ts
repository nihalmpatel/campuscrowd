import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit {
  username: String;
  password: String;

  constructor( 
    private validationService: ValidationService, 
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private userService: UserService,
    private router: Router 
  ) { }

  ngOnInit() {
    if(this.authService.loggedIn()){
      this.router.navigate(['/dashboard']);
    }
  }

  loginSubmit() {
    const user= {
      username:this.username,
      password:this.password
    }

    if(!this.validationService.validateLogin(user)) {
      this.flashMessage.show('All fields must be filled!',{cssClass: 'alert-danger', timeout:3000 });
      return false;
    }

    this.authService.login(user).subscribe(data=> {
      if(data.success){
        this.authService.storeUserData(data.token,data.user);
        
        this.userService.getUser(data.user.id).subscribe(data=>{
          if(!data.error){
            if(data.profile) {
              this.router.navigate(['/dashboard']);
            }
            else {
              this.router.navigate(['/createprofile']);
            }
          }
        });
      }
      else{
        if(data.error=="invalid_username") {
          this.flashMessage.show('Enter valid username!',{cssClass: 'alert-danger', timeout:3000 });
          return false;
        }
        if(data.error=="invalid_password") {
          this.flashMessage.show('Enter valid password!',{cssClass: 'alert-danger', timeout:3000 });
          return false;
        }
      }
    });
  }
}
