import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navsidebar',
  templateUrl: './navsidebar.component.html',
  styleUrls: ['./navsidebar.component.css']
})
export class NavsidebarComponent implements OnInit {
  user;
  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('auth_user'));
  }

  onLogoutClick(){
    this.authService.logoutUser();
    this.router.navigate(['/login']);
  }

}
