import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  username: String;
  password: String;
  email: String;
  cpassword: String;

  constructor( 
    private validationService: ValidationService, 
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if(this.authService.loggedIn()){
      this.router.navigate(['/dashboard']);
    }
  }

  signupSubmit() {
    const local={
      username: this.username,
      email: this.email,
      password: this.password,
      cpassword: this.cpassword,
    }

    if(!this.validationService.validateSignup(local)) {
       this.flashMessage.show('All fields must be filled!',{cssClass: 'alert-danger', timeout:3000 });
       return false;
    }

    if(!this.validationService.validateEmail(local.email)) {
      this.flashMessage.show('Enter valid email!',{cssClass: 'alert-danger', timeout:3000 });
      return false;
    }

    if(!this.validationService.validatePass(local)) {
      this.flashMessage.show('Password and Confirm Pasword should match!' ,{cssClass: 'alert-danger', timeout:3000 });
      return false;
    }

    delete local.cpassword;
    console.log(local);
    this.authService.signupUser(local).subscribe(data=> {
      if(data.success) {
        this.router.navigate(['/login']);
      }
      else {
        this.flashMessage.show('Signup failed!' ,{cssClass: 'alert-danger', timeout:3000 });
        return false;
      }
    });
  }

}
