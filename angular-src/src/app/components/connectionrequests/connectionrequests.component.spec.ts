import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionrequestsComponent } from './connectionrequests.component';

describe('ConnectionrequestsComponent', () => {
  let component: ConnectionrequestsComponent;
  let fixture: ComponentFixture<ConnectionrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectionrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
