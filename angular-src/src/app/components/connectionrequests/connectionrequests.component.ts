import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-connectionrequests',
  templateUrl: './connectionrequests.component.html',
  styleUrls: ['./connectionrequests.component.css']
})
export class ConnectionrequestsComponent implements OnInit {
  users=[]; // all users from connection requests
  curruser; // loggedIn User
  conn;

  constructor(
    private userService:UserService,
    private authService:AuthService,
  ) { }

  ngOnInit() {
    this.curruser=JSON.parse(localStorage.getItem('auth_user'));
    this.userService.getConnectionRequest(this.curruser.id).subscribe(data=>{
      this.users=data;
    });
  }

  onAcceptClick(to) {
    const data={
      from:this.curruser.id,
      to: to
    }
    
    this.userService.acceptConnection(data).subscribe(data=>{
      if(data.success) {
        this.conn=to;
      }
      else {
        console.log(data.error);
      }
    });    
  }

  onDeclineClick(to) {

  }

}
