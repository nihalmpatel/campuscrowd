import { Component, OnInit } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { UserService } from '../../services/user.service';
import { PostService } from '../../services/post.service';

import { NavsidebarComponent } from '../navsidebar/navsidebar.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  user;
  body;
  posts=[];

  constructor(
    private validationService: ValidationService,
    private userService: UserService,
    private postService: PostService,
  ) { }

  ngOnInit() {
    this.user=JSON.parse(localStorage.getItem('auth_user'));
    this.postService.getPosts().subscribe(data=>{
      //temp
      this.posts=data.reverse();
    });
  }

  postSubmit() {
    const post={
      body: this.body,
      id:   this.user.id, 
      name: this.user.fullname
    }
  
    if(!this.validationService.validatePost(post)) {
      alert('Enter valid post body!')
      return false;
    } 

    this.postService.addPost(post).subscribe(data=>{
      if(data.success){
        this.postService.getPosts().subscribe(data=>{
          //temp
          this.posts=data.reverse();
        });
      }
      else {
        alert('upload post failed!');
        return false;
      }
    });

  }

}
