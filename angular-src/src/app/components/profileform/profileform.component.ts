import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { ValidationService } from '../../services/validation.service';
import { UserService } from '../../services/user.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profileform',
  templateUrl: './profileform.component.html',
  styleUrls: ['./profileform.component.css']
})

export class ProfileformComponent implements OnInit {

  firstname;
  lastname;
  college;
  branch;
  year;
  city;
  state;
  country;
  bio;
  avatar;

  constructor(
    private validationService: ValidationService,
    private flashMessage: FlashMessagesService,
    private userService: UserService,
    private router: Router,
    private el: ElementRef
  ) { }

  ngOnInit() { }

  
  handleInputChange () {
   
  }

  profileSubmit(){

    const profile = {
      userid: JSON.parse(localStorage.getItem("auth_user")).id,
      firstname: this.firstname,
      lastname: this.lastname,
      college: this.college,
      branch: this.branch,
      year: this.year,
      city: this.city,
      state: this.state,
      country: this.country,
      bio: this.bio,
      avatar: '',
    }
  
    if(!this.validationService.validateProfile(profile)) {
      this.flashMessage.show('Mandatory fields should not be empty!' ,{cssClass: 'alert-danger', timeout:3000 });
      return false;
    }

    else{
      // Avatar Upload
      let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#avatar');
      let fileCount: number = inputEl.files.length;
      let formData = new FormData();
      if (fileCount > 0) { 
        formData.append('avatar', inputEl.files.item(0));
        // Upload avatar
        this.userService.uploadAvatar(formData).subscribe(data=>{
          if(data.success){
            profile.avatar=data.filename;
            // update avatar info in localstorage
            var localStorageUser=JSON.parse(localStorage.getItem("auth_user"));
            localStorageUser.avatar=data.filename;
            localStorage.setItem("auth_user",JSON.stringify(localStorageUser));
            // create the profile with avatar
            this.userService.createProfile(profile).subscribe(data=>{
              if(data.success){
                this.router.navigate(['/dashboard']);
              }
              else{
                return false;
              }
            });
          }
          else{
            alert('Failed uploading image!');
            return false;
          }
        });
      }

      else{
        // create Profile without avatar
        this.userService.createProfile(profile).subscribe(data=>{
          if(data.success){
            this.router.navigate(['/dashboard']);
          }
          else{
            return false;
          }
        });
      }
    }

  }

}
