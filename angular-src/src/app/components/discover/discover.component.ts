import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {
  users=[]; // all users
  curruser; // loggedIn User
  conn;
  
  constructor(
    private userService:UserService,
    private authService:AuthService,
  ) { }

  ngOnInit() {
    this.curruser=JSON.parse(localStorage.getItem('auth_user'));
    this.userService.getUsers().subscribe(data=>{
      //temp
      this.users=data;
    });
  }

  onConnectClick(to){  
    const data={
      from:this.curruser.id,
      to: to
    }
    this.userService.addConnection(data).subscribe(data=>{
      if(data.success) {
        this.conn=to;
      }
      else {
        console.log(data.error);
      }
    });    
  }

  onCancleClick(to) {
    const data={
      from:this.curruser.id,
      to: to
    }
    this.userService.cancleConnection(data).subscribe(data=>{
      if(data.success) {
        this.conn='';
      }
      else {
        console.log(data.error);
      }
    });    
  }

}
