// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';
import { FlashMessagesModule } from 'angular2-flash-messages';
// Services
import { ValidationService } from './services/validation.service';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { PostService } from './services/post.service';
import { AuthGuard } from './guards/auth.guards';
// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { NavsidebarComponent } from './components/navsidebar/navsidebar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileformComponent } from './components/profileform/profileform.component';
import { DiscoverComponent } from './components/discover/discover.component';
import { ConnectionrequestsComponent } from './components/connectionrequests/connectionrequests.component';

// Routes
const appRoutes: Routes = [
  { path: '', redirectTo:'login', pathMatch:'full' },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate:[AuthGuard] },
  { path: 'createprofile', component: ProfileformComponent, canActivate:[AuthGuard] },
  { path: 'discover', component: DiscoverComponent, canActivate:[AuthGuard] },
  { path: 'requests', component: ConnectionrequestsComponent, canActivate:[AuthGuard] },
]

// Declaration
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    NavsidebarComponent,
    DashboardComponent,
    HomeComponent,
    ProfileformComponent,
    DiscoverComponent,
    ConnectionrequestsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule,
  ],
  providers: [ValidationService,AuthService,AuthGuard,UserService,PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
