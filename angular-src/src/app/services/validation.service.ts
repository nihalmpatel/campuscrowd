import { Injectable } from '@angular/core';

@Injectable()
export class ValidationService {

  constructor() { }

  validateSignup(local) {
    if(local.username==undefined || !local.username.trim() || local.email==undefined || !local.email.trim() || local.password== undefined || !local.password.trim() || local.cpassword==undefined || !local.cpassword.trim()) {
      return false;
    }
    else {
      return true;
    }
  }

  validateLogin(local) {
    if(local.username==undefined || !local.username.trim() || local.password== undefined || !local.password.trim()) {
      return false;
    }
    else {
      return true;
    }
  }

  validateProfile(profile) {
    if(profile.firstname==undefined || !profile.firstname.trim() || profile.lastname== undefined || !profile.lastname.trim()) {
      return false;
    }
    else {
      return true;
    }
  }

  validatePost(post) {
    if(post.body==undefined || !post.body.trim()) {
      return false;
    }
    else {
      return true;
    }
  }

  validateEmail(email) {
    const re=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validatePass(local) {
    if(local.password!=local.cpassword) {
      return false; 
    }
    else {
      return true;
    }
  }

}
