import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { AuthService } from './auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class PostService {
  
  constructor( 
    private http: Http,
    private authService: AuthService
  ) { }

  addPost(post) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('posts/add'),post,{headers:headers})
    .map(res=>res.json());
  }

  getPosts() {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('posts/'),{headers:headers})
    .map(res=>res.json());
  }

}
