import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { AuthService } from './auth.service';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor( 
    private http: Http,
    private authService: AuthService
  ) { }

  getUser(id) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/'+id),{headers:headers})
    .map(res=>res.json())
  }

  createProfile(profile) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/createprofile'),profile,{headers:headers})
    .map(res=>res.json());
  }

  uploadAvatar(image) {
    let uploader:FileUploader = new FileUploader({url: this.authService.endpoint('users/uploadavatar'), itemAlias: 'avatar'});
    uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; }; 
    uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
      console.log("ImageUpload:uploaded:", item, status, response);
    };
    return this.http.post(this.authService.endpoint('users/uploadavatar'),image).map(res=>res.json());
  }

  getUsers() {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/'),{headers:headers})
    .map(res=>res.json());
  }

  addConnection(data){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/addconnection'),data,{headers:headers})
    .map(res=>res.json());
  }

  cancleConnection(data){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/cancleconnection'),data,{headers:headers})
    .map(res=>res.json());
  }

  getConnectionRequest(userid){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.get(this.authService.endpoint('users/connectionrequests/'+userid),{headers:headers})
    .map(res=>res.json());
  }

  acceptConnection(data){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return this.http.post(this.authService.endpoint('users/acceptconnection'),data,{headers:headers})
    .map(res=>res.json());
  }

}
