const express       = require('express');
const app           = express();
const mongoose      = require('mongoose');
const jwt           = require('jsonwebtoken');
const morgan        = require('morgan');
const bodyParser    = require('body-parser');
const cors          = require('cors');
const passport      = require('passport');
const configDB      = require('./config/database.js');
const port          = process.env.PORT || 8080;
const users         = require('./routes/users');
const posts         = require('./routes/posts');

// mongodb configuration 
mongoose.connect(configDB.url, {
  useMongoClient: true,
});

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(express.static('uploads'));
app.use(cors());
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

// routes
app.use('/users', users);
app.use('/posts', posts);

// launch 
app.listen(port);
console.log('Server is running on port:' + port);